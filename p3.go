package main

import (
	"fmt"
	"math"
	"time"
)

/*
* The prime factors of 13195 are 5, 7, 13 and 29.
*
* What is the largest prime factor of the number 600851475143 ?
 */

func f(n int) int {
	lastFactor := 1
	if n%2 == 0 {
		lastFactor = 2
		n = n / 2
		for n%2 == 0 {
			n = n / 2
		}
	}
	factor := 3
	maxFactor := int(math.Sqrt(float64(n)))
	for n > 1 && factor <= maxFactor {
		if n%factor == 0 {
			n = n / factor
			lastFactor = factor
			for n%factor == 0 {
				n = n / factor
			}
			maxFactor = int(math.Sqrt(float64(n)))
		}
		factor += 2
	}
	if n == 1 {
		return lastFactor
	} else {
		return n
	}
}

func main() {
	num := 600851475143
	start := time.Now()
	fmt.Println(f(num))
	fmt.Printf("run %f\n", time.Since(start).Seconds())
}
