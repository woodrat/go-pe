package main

import (
	"fmt"
	"math"
	"time"
)

/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

func divsible(num int, limit int) bool {
	for i := 1; i <= limit; i++ {
		if num%i != 0 {
			return false
		}
	}
	return true
}

func f1() int {
	ans := 0
	for i := 11; ; i++ {
		if divsible(i, 20) {
			ans = i
			break
		}
	}
	return ans
}

var primes = [...]int{2, 3, 5, 7, 11, 13, 17, 19}

func f2(limit int) int {
	prime_count := make(map[int]int)
	for _, i := range primes {
		prime_count[i] = 0
	}
	ans := 1
	for _, i := range primes {
		prime_count[i] = int(math.Floor(math.Log2(float64(limit)) / math.Log2(float64(i))))
		ans *= int(math.Pow(float64(i),float64(prime_count[i])))
	}
	fmt.Println(prime_count)
	return ans
}

func main() {
	start := time.Now()
	fmt.Println(f2(20))
	fmt.Printf("run %f\n", time.Since(start).Seconds())
}
