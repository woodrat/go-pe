package main

import (
	"fmt"
	"time"
)

func f(limit int) int {
	sum1, sum2 := 0, 0
	for i := 1; i <= limit; i++ {
		sum1 += i * i
		sum2 += i
	}
	return sum2*sum2 - sum1
}

func main() {
	start := time.Now()
	fmt.Println(f(100))
	fmt.Printf("run %f\n", time.Since(start).Seconds())
}
