package main

import (
	"fmt"
	"strconv"
	"time"
)

/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
*/

func is_palindrome(n int) bool {
	s := strconv.Itoa(n)
	len := len([]rune(s))
	for i := 0; i < len/2; i++ {
		if s[i] != s[len-i-1] {
			return false
		}
	}
	return true
}

func f1() int {
	ans := 0
	for i := 999; i >= 100; i-- {
		for j := 999; j >= 100; j-- {
			product := i*j
			if is_palindrome(product) {
				if (product > ans) {
					ans = product
				}
			}
		}
	}
	return ans
}

func f2() int {
	ans := 0
	a := 999
	var b,db int
	for  ;a >= 100;a-- {
		if a % 11 == 0 {
			b = 999
			db = 1
		} else {
			b = 990
			db = 11
		}
		for ;b>a;b-=db {
			if a*b <= ans {
				break
			}
			if is_palindrome(a*b) {
				ans = a*b
			}
		}
	}
	return ans
}

				
		
func main() {
	start := time.Now()
	fmt.Println(f2())
	fmt.Printf("run %f\n", time.Since(start).Seconds())
}
