package main

import (
	"math"
)

func is_prime(n int) bool {
	if n == 1 {
		return false
	} else if n < 4 {
		return true
	} else if n%2 == 0 {
		return false
	} else if n < 9 {
		return false
	} else if n%3 == 0 {
		return false
	} else {
		sqrt_n := int(math.Sqrt(float64(n)))
		div := 5
		for div <= sqrt_n {
			if n%div == 0 {
				return false
			}
			if n%(div+2) == 0 {
				return false
			}
			div += 6
		}
		return true
	}
	return true
}
