package main

import (
	"fmt"
	"time"
)

/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/

func solve1(limit int) int {
	sum1, sum2, sum3 := 0, 0, 0
	for i := 0; i < limit; i++ {
		if i%3 == 0 {
			sum1 += i
		}
		if i%5 == 0 {
			sum2 += i
		}
		if i%15 == 0 {
			sum3 += i
		}
	}
	return sum1 + sum2 - sum3
}

func solve2(limit int) int {
	sum_of_3 := 3 * (1 + (limit / 3)) * (limit / 3) / 2
	sum_of_5 := 5 * (1 + (limit / 5)) * (limit / 5) / 2
	sum_of_15 := 15 * (1 + (limit / 15)) * (limit / 15) / 2
	return sum_of_3 + sum_of_5 - sum_of_15
}

func main() {
	limit := 1000
	start := time.Now()
	fmt.Println(solve1(limit))
	time_1 := time.Since(start)
	fmt.Printf("%f s run solve1\n", time_1.Seconds())
	fmt.Println(solve2(limit))
	time_2 := time.Since(start) - time_1
	fmt.Printf("%f s run solve2\n", time_2.Seconds())
}
