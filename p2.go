package main

import (
	"fmt"
	"time"
)


func fib (limit int) chan int {
	c := make(chan int)
	go func() {
		for x, y:=0,1 ; x<limit && y < limit;  {
			x, y = y, x+y
			c <- x
		}
		close(c)
	}()
	return c
}

func main() {
	start := time.Now()
	limit := 4000000
	result := 0 
	for i := range fib(limit) {
		if i % 2 == 0 {
			result += i
		}
	}
	fmt.Println(result)
	fmt.Printf("run %f \n", time.Since(start).Seconds())
}
	
