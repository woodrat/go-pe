package main

import (
	"fmt"
	"math"
	"time"
)

func is_prime(n int) bool {
	if n == 1 {
		return false
	} else if n < 4 {
		return true
	} else if n%2 == 0 {
		return false
	} else if n < 9 {
		return false
	} else if n%3 == 0 {
		return false
	} else {
		sqrt_n := int(math.Sqrt(float64(n)))
		div := 5
		for div <= sqrt_n {
			if n%div == 0 {
				return false
			}
			if n%(div+2) == 0 {
				return false
			}
			div += 6
		}
		return true
	}
	return true
}

func nth_prime(n uint64) uint64 {
	primes := []int{2, 3, 5, 7}
	cnt := len(primes)
	for i := 11; ; i += 2 {
		if is_prime(i) {
			cnt++
		}

		if uint64(cnt) >= n {
			return uint64(i)
			break
		}
	}
	return 0
}

func main() {
	start := time.Now()
	fmt.Println("nst prime is : ", nth_prime(10001))
	fmt.Printf("run %f\n", time.Since(start).Seconds())
}
